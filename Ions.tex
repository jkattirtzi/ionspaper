\documentclass[manuscript = letter, journal= jpclcd, layout=standard]{achemso}
\setkeys{acs}{usetitle = true}
\usepackage{geometry}
\usepackage{times}
\usepackage{setspace}
\usepackage{subfigure}
\geometry{portrait,a4paper}
\usepackage{fancyhdr}
\usepackage[usenames,dvipsnames]{color}
\usepackage{amsmath,amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{tikz} 
\usepackage{bm}
\usepackage{enumerate}
\usepackage{psfrag}
\usepackage[version=3]{mhchem}
\usepackage{mathptmx} % makes maths font times too
\usepackage{multirow}
\numberwithin{equation}{section}
\newcommand{\tn}[1]{\textnormal{#1}}
\newcommand{\pkma}[1]{\textnormal{pK}_\textnormal{#1}}
\newcommand{\pka}[1]{$\pkma{#1}$}
\newcommand{\etal}{\textit{et al.} }
\newcommand{\kb}{k_\textnormal{B}}
\newcommand{\stnd}{^\circ}
\title{Ions at metal aqeuous interface}
\author{John A. Kattirtzi}
\email{Jakatt@mit.edu}
\author{David T. Limmer}
\author{Adam P. Willard}


\begin{document}
%\begin{abstract}
%my abstract
%\end{abstract}

%We want to understand how reactivity changes at interfaces in general for aqueous catalysis. Experimental probes offer reduce description of phenomena. Need better catalysts and these are currently under development. Used simulations, deifferent theories and specific techniques. Interfacial water structure and dyanics affect the stability and rate of change.

 
Interfacial chemistry is central to aqueous heterogeneous catalysis, where differences in reactivity from the bulk are exploited in order to increase the rate of reactions. A molecular understanding of how the structure and dynamics of the interface affect chemical reactions is still lacking. Experiments under ultra high vacuum conditions are able to probe the arrangements of molecules and these can be compared with high level quantum calculations \cite{Carrasco2012} but the connection to the condense phase is unclear. The development of surface sensitive probes such as SFG is essential for an empirical description. For a theoretical understanding, the currently available tools must be collated and applied accordingly. Here, we use a combination of different levels of theory, namely classical Molecular Dynamics (MD) and Density Functional Theory based Molecular Dynamics (DFTMD) combined with modern numerical techniques such as Markov State Modeling and Kinetic Monte Carlo to understand how the interfacial water structure and dynamics at a hydrated metal interface affects the mechanism of the association of ions. We analyze the thermodynamic stability and the kinetics of the association of two generic small hard ions (\ce{Na+} and \ce{I-}) and of the recombination of polar water ions (\ce{H3O+} and \ce{OH-}).

The characteristic difference between the structure of water molecules at a metal-water interface compared with the bulk is the strong preferential binding of water molecules, \cite{Roman2013} (approximately 0.4 eV) resulting in a two dimensional adlayer with hydrogen bonds (approximately 0.25 eV) directed along this plane. \cite{Thiel1987,  Siepmann1995, Michaelides2003} Frustrations of the hydrogen bond network in local regions in the adlayer cause heterogeneities in the structure and the dynamics. \cite{Limmer2013} The time scale for the relaxation of the adlayer lies in the order of tens of nano seconds, whilst bulk fluctuations typically lie in the order of pico seconds. This result affects the dynamics of water molecules above the adlayer. Overall, the interactions of the adlayer with the surrounding water molecules can be described as hydrophobic and can attract defects such as protons from the bulk or weakly solvated ions.  These processes are able to stabilize transition states leading to significant changes in reactivity. 

From the perspective of a molecular simulation, reactants are extremely long lived with respect to currently available simulation time. For this reason, chemical reactions are viewed as rare (albeit important) events and thus a single trajectory will not give direct information on factors that influence chemical reactivity. Simulating states along a reaction coordinate provides more useful information. This can be done using umbrella sampling, in conjunction with transition path sampling to find the relevant reaction coordinate. More recently, Hummer \etal \cite{Rosta2015} developed the dynamic histogram analysis method (DHAM) that uses umbrella sampling with a Markov State Model to calculate the free energy change for the reaction. It would be possible to obtain all trajectories purely from DFTMD but the gain in accuracy may be countered by the cost in the calculation resulting in a lack of sampling. In cases where bonds are not made or broken and there is not a significant change in the electronic structure the classical approximation used in molecular dynamics is sufficient. 

Previous studies have successfully used a classical model for describing the hydrated metal and find that it accurately recovers experimentally known quantities such as the point of zero charge. \cite{Willard2009} The association of hard ions is dependent on the electrostatic contributions of many water molecules and has been successfully studied using MD in bulk water. \cite{Ballard2012} Conversely, DFTMD is required to in order for protons to hop between molecules during the recombination of water ions \cite{Geissler2001, Marx1998} and there are fewer water molecules that contribute electrostatically. \cite{Reischl2009}   We therefore use a QM/MM approach where the hydrated metal-water interactions are the same as the hard ions system but the water-water interaction and the water ion interactions are treated at the DFT level. The adsorption of water ions in the adlayer is known to have a significant effect on the electronic structure of the hydrated metal and as the models used here would not be suitable to account for this, such simulations have not been considered in this study. 

The reactions investigated here can be viewed as archetypes of reactions that occur both in bulk and at electrochemical interfaces. Hard ions are typical in aqueous electrolytes and the recombination of water ions is important in understanding proton transfer. The hard ions are simulated at different distances using umbrella sampling, going from an associated state to a dissociated state. The top $3$ panels on the right in figure \ref{fig1} show three snapshots at different distances. The classical ions (represented by van der waals spheres) are surrounded by water molecules. Fluctuations in the solvent play a role in the stability of the ions through electrostatic contributions. The water molecules themselves do not directly participate in the reaction. This is in contrast to the recombination of the \ce{H3O+} and \ce{OH-} where surrounding water molecules play a direct role. The \ce{H3O+} and \ce{OH-} ions have been highlighted in green and yellow respectively in the top left panels in figure \ref{fig1}. In the red panel the ions are separated by $2$ water molecules that form a hydrogen bond wire. In the blue and black panels the identity of the \ce{H3O+} have shifted as a proton has jumped onto a neighbouring water molecule. Hassanalli \etal have shown that recombination occurs after a collective motion of the compression of the wire, which is followed by a triple proton hop resulting in neutral water molecules.\cite{Hassanali2011} The simulations carried out in this work are analogous to those previously carried out in bulk liquid by Hassanalli \etal where recombination trajectories are sampled over $400$ initial conditions. In both reactions, the differences in the number of water molecules, their structure and their orientation at the interface suggests that a mechanistic difference may be expected from the bulk. An ensemble of interface trajectories is compared with an ensemble of bulk trajectories for both the recombination of the water ions and the hard ions. 

For a first analysis comparing the thermodynamics of the two reactions the euclidean distance between the two ions $R$ is used. This is readily available for classical ions by taking their atomic centers. Using umbrella sampling, the free energy profile as a function of $R$ for the two classical ions is calculated as:
\begin{eqnarray}
\beta F(R)= -\ln [\langle \delta (R'-R) \rangle]
\end{eqnarray}
where $\beta=1/k_B T $ with Boltzmann's constant  $k_B$ and temperature $T$. This is the potential of mean force that has been calculated in the past. To calculate $R$ for the water ions the identity of the \ce{H3O+} and \ce{OH-} species are first determined based on the oxygen-hydrogen coordination as outlined in the supporting information. $R$ is then determined using the oxygen centers. Markov states are discretized from the $R$ values sampled and the Markov transition probability matrix, $M_{ij}$ is constructed:
\begin{eqnarray}
M_{ij}= \frac{\sum^{N_{sim}}_{k_s} T_{ij}}{\sum^{N_{sim}}_{k_s} n_i^{k_s}}
\end{eqnarray}
where $k_s$ is the simulation, which is summed over and $i$ and $j$ are the Markov states. $T_{ij}$ is the number of transitions from state $i$ to state $j$ and $n_i^{k_s}$ are the total number counts in simulation $k_s$. The free energy profile is given by:
\begin{eqnarray}
F(R)= - k_B T \ln M_{ij} 
\end{eqnarray}
after taking into account the oreintational entropy correction term $-2\ln R$. The bottom panels in figure \ref{fig1} show the free energy profiles for the classical ions (on the right) and for the water ions (on the left) in the bulk (red) and at the interface (blue). The hard ions show excellent agreement between the interface and the bulk. The barrier height is within (0.2 KT?) at approximately $3.5 \textnormal{ \AA} $  and the depth of the basin at $2.5 \textnormal{ \AA } $ matches almost exactly. This is likely to be because dissociation is driven through electrostatic contributions which are not reflected in the potential of mean force [NOT SURE IF SHOULD SAY: which are long ranged and the interface contributions are small by comparison]. The barrier height for the recombination of the water ions is increased at the interface by approximately (2KT?). The neutralized recombined state is not sampled and so the relative pK$_{w}$ for the interface and bulk is not accessible. Based on the data available, the similarity in the thermodynamics for both hard ions and water ions at the interface and in the bulk suggests that the kinetics for both systems should be very similar. This is unexpected and contrary to previous studies that show the dynamics of water to be different at the interface compared with the bulk. 

To further investigate the dynamics of the classical ions we define the dynamical indicator functions $h^{c}(t)$ and $g^{c}(t)$, where $h^{c}(t)=1$ and $g^{c}(t)=0$ if the ions are associated and $h^{c}(t)=0$ and $g^{c}(t)=1$ otherwise. The flux-flux correlation function is then given by:
\begin{eqnarray}
C(t)=\frac{\langle h^c g^c\rangle}{\langle h^c\rangle}.
\end{eqnarray}
The time derivative $dC(t)/dt$ reaches the rate constant $k$ at long time. Despite the similarity in the potential of mean force for the dissociation of the hard ions between the interface and the bulk, analysis of the gradient of the flux-flux correlation function, as shown in figure \ref{fig2} indicates that the process in the bulk occurs approximately $80$ times faster than at the interface. The indicator functions are also used to give the instanton time:
\begin{equation}
\Delta t = \int ^{t_{diss}}_{0} (1-h^{c}) (1-g^{c}) dt
\end{equation} 
where $t_{diss}$ is the time for dissociation. The instanton distribution times shown in the insert are significantly longer at the interface compared to the bulk. This suggests that the dynamics of the surrounding water molecules have been slowed down at the interface and that they play an important role in the reaction. Previous studies have also confirmed that the interionic distance ($R$) alone is not a good reaction coordinate for describing the dissociation of similar hard ions (\ce{Na+} and \ce{Cl-}) in bulk water. Geissler \etal found that the relaxation dynamics of the solvent around the ions is slower than the dynamics of the solute and that a reaction coordinates needs to take into account the solvent.\cite{Geissler1999}  More recent work on a similar system by Dellago \etal also showed that the solvent contributions are long ranged, extending up to the third solvation shell and that both steric effects and electrostatics contribute to the reaction coordinate. \cite{Ballard2012}

Taking a similar approach to previous studies, we apply transition path sampling to compute the transition state ensemble. The Madelung potential is defined as:
\begin{eqnarray}
\psi= \sum_{i>2} \frac{q^{c}_i}{r_{\alpha i}} - \sum_{i>2} \frac{q^{c}_i}{r_{\beta i}}
\end{eqnarray}
where $\alpha$ and $\beta$ represent the ions and $i$ is the index for all the other atoms with the classical charge $q^{c}$. This is the electrostatic potential that is experienced by the ions and has been used successfully in MD simulations to understand solvation effects. \cite{Willard2009, Reed2008} Figure \ref{fig2} shows the projection of $\psi$ along the interionic distance. The transition states for different trajectories are represented by black circles, all of which lie in a similar range of $\psi$ (with respect to the range over the entire reaction) whilst the interionic range is extremely broad. It is confirmed through committer analysis (shown in the supplementary information) that $\psi$ is a suitable reaction coordinate for the dissociation of classical ions. 

Using the reaction coordinate $\psi$, the free energy profile is given by:
\begin{eqnarray}
F(\psi)=-k_B T \ln P(\psi)
\end{eqnarray}
where $P(\psi)$ is the probability of $\psi$ and this is shown in figure \ref{fig2} comparing the bulk to the interface. For both the bulk and the interface, the basins of the associated and dissociated states are in similar positions but the barrier height at the interface is almost $2.5$ times larger than at the bulk. This increase is due to local fields from molecules within the first solvation shell. It is clear that the overall decrease in the rate of dissociation is due to both the decrease in the kinetics of water molecules and the increasing thermodynamic reaction barrier. 

The case is very different for the recombination of water ions. The recombination time $\tau_{DFT}$ is obtained by monitoring the number of ions in the DFT simulation as was done in reference \citep{Hassanali2011} and outlined in the supplementary information. However, even with $400$ DFTMD simulations of each system a comparison of the distribution of recombination times between the bulk and the interface is difficult due to the lack of sampling. We therefore used $\tau_{DFT}$ to parameterise a Kinetic Monte Carlo model and analyse $\tau_{KMC}$. The numerical model takes in a set of DFTMD configurations as lattice sites. A hydronium and a hydroxide are initially identified using the same procedure that was used to calculate $R$. A proton is able to either jump from the \ce{H3O+} or to the \ce{OH-} with rate constant $k_1$. Alternatively if the \ce{H3O+} and \ce{OH-} ions are within three hydrogen bonds a proton may triple jump (resulting in recombination) with rate constant $k_2$. The rate constants $k_1$ and $k_2$ are parameterized using a maximum likelihood approach based on the DFTMD simulations. The recombination time $\tau_{KMC}$ is then obtained by summing all the timesteps until recombination. The time corresponding to each step is determined as:
\begin{eqnarray}
dt=-\ln U/ k_N
\end{eqnarray}
where U is a random number between $0$ and $1$ and $k_N$ is the sum of rates over all possible events. The efficiency of the numerical model allows for hundreds of thousands of recombination simulations. Using this it is possible to determine the probability distribution of $\tau_{KMC}$ and the results corresponding to the bulk and the interface are shown in figure \ref{fig3}. The probability distribution of fast reaction times are very similar in the bulk and at the interface, whilst larger differences can be seen at longer reaction times.

To analyze the dynamics of the water molecules the dynamical variables $h^{w}(t)$ and $g^{w}(t)$ are defined similar to before, where $h^{w}(t)=1$ and $g^{w}(t)=0$ prior to recombination and $h^{w}(t)=0$ and $g^{w}(t)=1$ after the recombination of ions. The instanton time $\Delta t_w$ is calculated by
\begin{equation}
\Delta t_w = \int ^{\tau}_{0} (1-h_{w}) (1-g_{w}) dt
\end{equation} 
and their distribution is shown in the insert of \ref{fig3}. The surface instanton times are significantly shorter than the bulk instanton times and overcome the increased thermodynamic barrier at the surface that was observed in figure \ref{fig1}. This results in the similarity in the recombination times. 

To investigate the differences in the electrostatic environment we define the charge parameter:
\begin{eqnarray}
Q=\frac{\sum_i | q^{d}_{\ce{H3O+}}| +\sum_i | q^{d}_{\ce{OH-}}    |}{ \sum_i | q^{d}_{\ce{H3O+}}| \times \sum_i | q^{d}_{\ce{OH-}}    | }
\end{eqnarray}
and $q^d$ is the Mulliken charge on the atom. This gives $Q$ to be the normalized Mulliken charge of the system where $1$ is the average sum of \ce{H3O+} and \ce{OH-} ions and $0$ is the average sum of neutral water molecules. It is evident from figure \ref{fig3} that the charge of the ions are very similar in both interface and bulk systems. This implies that the free energy to stabilize the reactants and products are very similar for both the interface and the bulk. The longer tails in the distribution of the recombination times for the interface system are therefore not due to different electrostatics but to the different structure and dynamics of the water molecules at the interface. Further analysis showed that the underlying surface heterogeneities in the structure of adsorbed water molecules are reflected in the long time tails. 

Unlike classical ions, protons only move through hydrogen bond pathways. The hydrogen bond correlation function:
\begin{eqnarray}
C_{HB} = \frac{\langle h_{HB}(0) \rangle \langle h_{HB} (t) \rangle } {\langle h_{HB} \rangle}
\end{eqnarray}
where $\langle h_{HB}(0) \rangle $ is the set of hydrogen bonds at $t=0$ and this is normalised by the number of existing hydrogen bonds $\langle h_{HB} \rangle$, quantifies the hydrogen bond network. This is shown in figure \ref{fig3} where it can be seen that the paths for proton transfer are long lasting relative to the recombination times.  In the first $5$ ps the lifetime of the hydrogen bonds are similar and this matches the distribution of the rates at short times. 


To summarize, contemporary tools have been used to understand rare events and to analyze the thermodynamics and kinetics of the dissociation of hard ions and the recombination of water ions. We find that a suitable reaction coordinate for the dissociation of hard ions is the Madelung potential. Using this we show that the thermodynamic barrier has been increased at the interface. Additionally, the kinetics of water molecules are slower. This results in dissociation rates that are $80$ times slower than at the bulk. The recombination of water ions is significantly different. The average recombination rates are very similar at the interface to the bulk. The thermodynamic profile as a function of interionic distance shows a slightly higher barrier at the interface, compared with the bulk, but this is offset by the faster kinetics as inferred from the instanton times. The relatively long lasting hydrogen bond pathways allow for the faster dynamics and the differences observed at longer time scales correspond well with underlying surface heterogeneities. Further work on different systems comparing the interface to the bulk may show interesting mechanistic differences.

From this study, it is clear that chemical reactivity at an interface can be significantly different from the bulk but analysis from standard molecular dynamics techniques is not straight forward. The interface directly affects the structure of solvent water molecules but the long range electrostatic contributions can be hard to characterize. Only considering thermodynamics is not sufficient to understand the differences as the kinetics may also be very different. 

\clearpage
\begin{figure}
\includegraphics[scale=0.5]{fig1-crop.pdf} 
\caption{\label{fig1} Typical snapshots going from a recombined state (black left panel) to a dissociated state (red right panel) for water ions (left) and classical ions (right). The positive ion is highlighted in green and the negatively charged ion is highlighted in yellow. The free energy profile as a function of $R$ is shown in the bottom panels with red for bulk and blue for the interface.  }
\end{figure}
\begin{figure}
\includegraphics[scale=0.5]{fig2-crop.pdf}
 \caption{\label{fig2} Flux-flux correlation function for the classical ions in the top panel with the distribution of the intanston times in the inset. The madelung potentials of the transition states as a function of interionic distance are shown in the bottom left panel. The free energy as a function of madelung potential is shown in the bottom right panel.}
\end{figure}
\begin{figure}
\includegraphics[scale=0.5]{fig3-crop.pdf} 
\caption{\label{fig3} The distributions of the $\tau_{KMC}$ for the bulk (red) and the interface (blue) with the inset showing the instanton times. [DAVID please check colors for instanton times]. The free energy as a function of charge parameter is shown in the bottom left and the hydrogen bond correlation function is shown in the bottom left.}
\end{figure}
\clearpage
\section{Supporting Information}

\subsection{DFT Simulations}

Density Functional Theory based Molecular Dynamics calculations were performed with the Born-Oppenheimer Molecular Dynamics method implementation in the freely available program package CP$2$K/Quickstep. \cite{CP2K, VandeVondele2005}  The implementation includes the Gaussian Plane Wave (GPW) scheme, which requires both Gaussian and plane wave basis sets. \cite{Lippert1997} A double-$\zeta$ basis set with one set of polarised functions (DZVP) was used. \cite{vandevondele2007} For the plane wave basis set a cut off at 280 Ry was used for bulk calculations and a larger cut off at 300 Ry was used for the interface calculation. Only the $\Gamma$ point of the supercell was used. The core electrons are represented by analytical Goedecker-Tetter-Hutter (GTH) pseudopotentials, \cite{goedecker1996,hartwigsen1998}. The Perdew-Burke-Erzenhof (PBE) \cite{Perdew1996} functional was used and Grimme's D3 dispersion corrections were included for calculation of the interface. \cite{Grimme2010}

The IC-QM/MM method was used for the interface calculations. \cite{Golze2013a} In this approach the metal atoms are treated at the MM level, where the charges are allowed to fluctuate due to image charges, whilst a constant potential is fixed. The water molecules interact with the metal using a three body potential. \cite{Siepmann1995} The (111) geometry of the metal, using the Pt lattice constant, was kept fixed. Water ion interactions with the metal are not considered. 

The DFT simulations used a time step of $0.5$ fs and with a target temperature of $330$ K, which was controlled using the canonical sampling velocity rescaling thermostat in the canonical ensemble NVT.\cite{Bussi2007} These were chosen as they have been chosen to be stable for the IC-QM/MM method. 

The bulk systems consisted of $32$ and $64$ water molecules inside cubes of length $9.8528 $ \AA $\;$ and $12.4138$ \AA $\;$ respectively. An orthorhombic supercell was constructed for the interface system by taking a sub set of atoms from a larger equilibrated classical constant potential simulation by only including atoms in a box $13.8987 \times 14.4430 \times 22.0$ \AA $\;$ and then adding a $20$ \AA $\;$ vacuum buffer. The number of water molecules in the system were: $106$,  $98$ and $96$. Periodic boundary conditions were applied in all three dimensions. 

The starting configurations correspond to classically equilibrated wate configurations. These were then equilibrated at the DFT level for $10$ ps after which \ce{H3O+} and \ce{OH-} ions were inserted by inserting and removing a proton from two water molecules. The systems were then equilibrated for $6$ ps and simulated for a further $10$ ps using coordination constraints on the ions in the form of the switching function:
$\sum^{N_H}_{O^{\ce{OH-} \textnormal{or } \ce{H3O+}}} \frac{1-(r/r_0)^{16}} {1-(r/r_0)^{56}}$
with $r=1.32$ \AA $\;$. Initial configurations from these were taken every $100$ fs, after the initial $6$ ps equilibration. The recombination trajectories were then simulated with no constraints. The combined simulation time is approximately $1$ ns, which is at least an order of magnitude higher than standard DFTMD studies. 


\subsection{Identifying Water Ions}
To identify the \ce{H3O+} and \ce{OH-} species in the DFTMD simulation the coordination of the hydrogen atom with an oxygen atom is calculated as:
\begin{eqnarray}
n_{OH}(r)= (1-(r/r_0)^{16})/(1-(r/r_0)^{56})
\end{eqnarray}
from reference \cite{Hassanali2011}, where $r$ is the distance between the \ce{H} atom and the \ce{O} atom and $r_0=1.32 \AA$. Calculating $n_{OH}$ for every oxygen atom with each hydrogen atom and taking the maximum value allows for the assignment of each hydrogen atom with one oxygen atom. The oxygen atom that coordinates with three hydrogen atoms is the \ce{H3O+} center and the oxygen atom that only coordinates with one hydrogen atom is the \ce{OH-}. In some instances the distances can be very delocalised and more than one \ce{H3O+} and \ce{OH-} centers are identified. In these cases a cluster of ions can be identified and the center of the cluster is the charge center. 
\subsection{Recombination time from DFTMD simulations}
The number of ions in the simulation is obtained using the same order parameter as in reference \citep{Hassanali2011} where the number of ions:
\begin{eqnarray}
N_{ions}=\sum_{O}^{N_{O}}(n_O(r)-2)^2 ,
\label{eqn:Nions}
\end{eqnarray}
$N_{O}$ is the total number of oxygen atoms and 
\begin{eqnarray}
n_O(r)=\sum_{O^{\ce{OH-} or \ce{H3O+} }}^{N_H} (1-(r_{OH}/r_0)^{16})/(1-(r/r_0)^{56}). 
\end{eqnarray}
The recombination time for a given simulation is obtained as the first instance in time where $N_{ions}$ approximately equals $0$.

\subsection{Water ions mechanism}
The mechanism for the recombination of water ions in bulk is known to occur after the contraction of a hydrogen bond wire. The same is found for the recombination at the interface. Figure \ref{Waterrec} shows the number of ions in a typical simulation of recombination at the bulk and at the interface as determined by equation \ref{eqn:Nions}. The time origin has been shifted so that $t=0$ is the time recombination has occurred. The panel on the right shows the corresponding hyrdrogen bond length. In both cases the contraction of the wire before recombination is identified.  
\begin{figure}
\includegraphics[scale=0.5]{Watermechs-crop.pdf} 
\caption{\label{Waterrec} The recombination of water ions is followed by analysing the number of ions in the simulation as seen for a typical trajectory in the panel on the left. The recombination time is shifted to $t=0$. The panel on the right shows that the hydrogen bond wire contracts before recombination for both the interface and the bulk. }
\end{figure}

\subsection{Surface Heterogeneity }

Classical simulations of Pt-water interface showed heterogeneities in the water mobility above the interface. \cite{Limmer2013} Two small regions (Interface A and Interface B) were isolated from the larger classical interface and the recombination of water ions were investigated. DFTMD simulations were combined with the Kinetic Monte Carlo model. The distribution of the KMC recombination times are shown in figure \ref{PTratesAB} where similar behavior is observed at short times and differences can be observed in the tails of the distributions. 

\begin{figure}
\includegraphics[scale=0.5]{PtratesKMCAB2.pdf} 
\caption{\label{PTratesAB} The recombination of water ions at two different interfaces is investigated using DFTMD simulations and the Kinetic Monte Carlo Model. The distributions of the rates show similar behavior at quick recombination times but differences are observed in the tails. }
\end{figure}

\subsection{Kinetic Monte Carlo}
A numerical model based on the Kinetic Monte Carlo algorithm was implemented. The model takes static configurations from the DFT trajectories. The hydrogen bond network is analyzed using a standard definition where the \ce{O-O} length must be less than $3.5$ A and the \ce{OHO} angle must be less than $30^\circ$. A proton can hop to or from the \ce{H3O+} and \ce{OH-} sites using the structural diffusion rate parameter $k_1$. Recombination through a water wire is allowed if the \ce{H3O+} and \ce{OH-} are connected through $3$ hydrogen bond lengths using the rapid recombination rate parameter $k_2$, which is $0$ if the are ions are not connected through $3$ hydrogen bond lengths. Recombination is also allowed through structural diffusion if the result after the hop is a neutral species. A screened coulombic attraction has been included in the structural diffusion parameter by using the experimental value of the dielectric constant of water. 

Initial values of $k_1$ and $k_2$ taken from previous studies, were then optimised using the maximum likelihood estimation with the DFTMD simulations. The optimal values for the bulk were $k_1=1150$ fs and $k_2=325$ fs and for the interface $k_1=1075$ fs and $k_2=500$ fs.

The value of the dielectric constant and the definition used for the hydrogen bond network were tested. For the dielectric constant the PBE value of $67$ was tried. \cite{Sharma2007} Whilst these do play a role, no noticeable differences could be observed for the probability distributions shown, most likely due to the rapid recombination.

 

\subsection{Markov State Model}
[Not sure how bins were discritzed from time vs distance data]. What else needs to be added here?

\subsection{Classical Simulations}

\subsection{Committer Analysis}
\clearpage
 \bibliography{Intro}
\bibliographystyle{plain}
\end{document}