\providecommand{\latin}[1]{#1}
\providecommand*\mcitethebibliography{\thebibliography}
\csname @ifundefined\endcsname{endmcitethebibliography}
  {\let\endmcitethebibliography\endthebibliography}{}
\begin{mcitethebibliography}{13}
\providecommand*\natexlab[1]{#1}
\providecommand*\mciteSetBstSublistMode[1]{}
\providecommand*\mciteSetBstMaxWidthForm[2]{}
\providecommand*\mciteBstWouldAddEndPuncttrue
  {\def\EndOfBibitem{\unskip.}}
\providecommand*\mciteBstWouldAddEndPunctfalse
  {\let\EndOfBibitem\relax}
\providecommand*\mciteSetBstMidEndSepPunct[3]{}
\providecommand*\mciteSetBstSublistLabelBeginEnd[3]{}
\providecommand*\EndOfBibitem{}
\mciteSetBstSublistMode{f}
\mciteSetBstMaxWidthForm{subitem}{(\alph{mcitesubitemcount})}
\mciteSetBstSublistLabelBeginEnd
  {\mcitemaxwidthsubitemform\space}
  {\relax}
  {\relax}

\bibitem[Carrasco \latin{et~al.}(2012)Carrasco, Hodgson, and
  Michaelides]{Carrasco2012}
Carrasco,~J.; Hodgson,~A.; Michaelides,~A. {A molecular perspective of water at
  metal interfaces}. \emph{Nature Materials} \textbf{2012}, \emph{11},
  667--674\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Roman and Gro\ss(2013)Roman, and Gro\ss]{Roman2013}
Roman,~T.; Gro\ss,~A. {Structure of water layers on hydrogen-covered Pt
  electrodes}. \emph{Catalysis Today} \textbf{2013}, \emph{202}, 183--190\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Thiel and Madey(1987)Thiel, and Madey]{Thiel1987}
Thiel,~P.~a.; Madey,~T.~E. {The interaction of water with solid surfaces:
  Fundamental aspects}. \emph{Surface Science Reports} \textbf{1987}, \emph{7},
  211--385\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Michaelides \latin{et~al.}(2003)Michaelides, Ranea, de~Andres, and
  King]{Michaelides2003}
Michaelides,~A.; Ranea,~V.~A.; de~Andres,~P.~L.; King,~D.~A. {General Model for
  Water Monomer Adsorption on Close-Packed Transition and Noble Metal
  Surfaces}. \emph{Physical Review Letters} \textbf{2003}, \emph{90},
  216102\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Siepmann and Sprik(1995)Siepmann, and Sprik]{Siepmann1995}
Siepmann,~J.~I.; Sprik,~M. {Influence of surface topology and electrostatic
  potential on water/electrode systems}. \emph{J. Chem. Phys.} \textbf{1995},
  \emph{102}, 511--524\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Willard \latin{et~al.}(2009)Willard, Reed, Madden, and
  Chandler]{Willard2009}
Willard,~A.~P.; Reed,~S.~K.; Madden,~P.~A.; Chandler,~D. {Water at an
  electrochemical interface-a simulation study}. \emph{Faraday Discuss}
  \textbf{2009}, \emph{141}, 423--441\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Limmer \latin{et~al.}(2013)Limmer, Willard, Madden, and
  Chandler]{Limmer2013}
Limmer,~D.~T.; Willard,~a.~P.; Madden,~P.; Chandler,~D. {Hydration of metal
  surfaces can be dynamically heterogeneous and hydrophobic}. \emph{Proceedings
  of the National Academy of Sciences} \textbf{2013}, \emph{110},
  4200--4205\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Geissler \latin{et~al.}(2001)Geissler, Dellago, Chandler, Hutter, and
  Parrinello]{Geissler2001}
Geissler,~P.~L.; Dellago,~C.; Chandler,~D.; Hutter,~J.; Parrinello,~M.
  {Autoionization in liquid water.} \emph{Science (New York, N.Y.)}
  \textbf{2001}, \emph{291}, 2121--4\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Marx \latin{et~al.}(1998)Marx, Tuckerman, Hutter, and
  Parrinello]{Marx1998}
Marx,~D.; Tuckerman,~M.~E.; Hutter,~J.; Parrinello,~M. {The nature of the
  hydrated excess proton in water}. \emph{Nature} \textbf{1998}, \emph{397},
  601--604\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Hassanali \latin{et~al.}(2011)Hassanali, Prakash, Eshet, and
  Parrinello]{Hassanali2011}
Hassanali,~A.; Prakash,~M.~K.; Eshet,~H.; Parrinello,~M. {On the recombination
  of hydronium and hydroxide ions in water}. \emph{Proceedings of the National
  Academy of Sciences of the United States of America} \textbf{2011}, \relax
\mciteBstWouldAddEndPunctfalse
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Reischl \latin{et~al.}(2009)Reischl, K\"{o}finger, and
  Dellago]{Reischl2009}
Reischl,~B.; K\"{o}finger,~J.; Dellago,~C. {The statistics of electric field
  fluctuations in liquid water}. \emph{Molecular Physics} \textbf{2009},
  \emph{107}, 495--502\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Ballard and Dellago(2012)Ballard, and Dellago]{Ballard2012}
Ballard,~a.~J.; Dellago,~C. {Toward the mechanism of ionic dissociation in
  water}. \emph{J. Phys. Chem. B} \textbf{2012}, \emph{116}, 13490--13497\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\end{mcitethebibliography}
